package org.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    /**
     * Handles the root ("/") GET request by returning a greeting message.
     *
     * @return "Hello World"
     */
    @GetMapping("/")
    public String helloWorld() {
        return "Hello World";
    }
}
